package com.kzawilski.applause.service;

import com.kzawilski.applause.domain.dto.TesterWithBugCountDTO;
import com.kzawilski.applause.repository.TesterRepository;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BugCounterService {

    private TesterRepository testerRepository;

    public BugCounterService(TesterRepository testerRepository) {
        this.testerRepository = testerRepository;
    }


    public List<TesterWithBugCountDTO> getTestersWithBugCount(String[] country, String[] device) {
        if (country == null || (country.length == 1 && country[0].equals("ALL"))) {
            country = new String[]{};
        }

        if (device == null || (device.length == 1 && device[0].equals("ALL"))) {
            device = new String[]{};
        }

        List<Object[]> rawTesterWithBug;
        if (country.length == 0 && device.length == 0) {
            rawTesterWithBug = testerRepository.findBugCountForTester();
        } else if (device.length == 0) {
            rawTesterWithBug = testerRepository.findBugCountForTesterByCounty(country);
        } else if (country.length == 0) {
            rawTesterWithBug = testerRepository.findBugCountForTesterByDevice(device);
        } else {
            rawTesterWithBug = testerRepository.findBugCountForTesterByCountyAndDevice(country, device);
        }

        return prepareDTO(rawTesterWithBug);

    }

    private List<TesterWithBugCountDTO> prepareDTO(List<Object[]> rawTesterWithBug) {
        return rawTesterWithBug
                .stream()
                .map(raw -> new TesterWithBugCountDTO(
                        testerRepository.findById(((BigInteger) raw[0]).longValue()).get(),
                        ((BigInteger) raw[1]).longValue()
                ))
                .collect(Collectors.toList());
    }
}
