package com.kzawilski.applause;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.kzawilski.applause.domain.entity.Bug;
import com.kzawilski.applause.domain.entity.Device;
import com.kzawilski.applause.domain.entity.Tester;
import com.kzawilski.applause.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

@Component
public class SampleDataLoader implements ApplicationRunner {

    private static final Logger log = LoggerFactory.getLogger(SampleDataLoader.class);


    private TesterRepository testerRepository;
    private BugRepository bugRepository;
    private DeviceRepository deviceRepository;

    public SampleDataLoader(TesterRepository testerRepository, BugRepository bugRepository, DeviceRepository deviceRepository) {
        this.testerRepository = testerRepository;
        this.bugRepository = bugRepository;
        this.deviceRepository = deviceRepository;
    }

    @Autowired
    public void run(ApplicationArguments args) {
        List<Tester> testers = loadObjectList(Tester.class, "testers.csv");
        for (Tester tester : testers) {
            testerRepository.save(tester);
        }

        List<Bug> bugs = loadObjectList(Bug.class, "bugs.csv");
        for (Bug bug : bugs) {
            bugRepository.save(bug);
        }

        List<Device> devices = loadObjectList(Device.class, "devices.csv");
        for (Device device : devices) {
            deviceRepository.save(device);
        }
    }

    private <T> List<T> loadObjectList(Class<T> type, String fileName) {
        try {
            CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
            CsvMapper mapper = new CsvMapper();
            mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"));
            File file = new ClassPathResource(fileName).getFile();
            MappingIterator<T> readValues =
                    mapper.readerFor(type).with(bootstrapSchema).readValues(file);
            return readValues.readAll();
        } catch (Exception e) {
            log.error("Error occurred while loading object list from file " + fileName, e);
            return Collections.emptyList();
        }
    }
}