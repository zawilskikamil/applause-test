package com.kzawilski.applause.repository;

import com.kzawilski.applause.domain.entity.Tester;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TesterRepository extends JpaRepository<Tester, Long> {

    @Query(value =
            "SELECT T.TESTER_ID, COUNT(B.BUG_ID) " +
                    "FROM TESTER T " +
                    "JOIN BUG B ON T.TESTER_ID = B.TESTER_ID " +
                    "JOIN DEVICE D ON B.DEVICE_ID = D.DEVICE_ID " +
                    "WHERE T.COUNTRY IN :country AND D.DESCRIPTION IN :device " +
                    "GROUP BY T.TESTER_ID",
            nativeQuery = true)
    List<Object[]> findBugCountForTesterByCountyAndDevice(String[] country, String[] device);

    @Query(value =
            "SELECT T.TESTER_ID, COUNT(B.BUG_ID) " +
                    "FROM TESTER T " +
                    "JOIN BUG B ON T.TESTER_ID = B.TESTER_ID " +
                    "JOIN DEVICE D ON B.DEVICE_ID = D.DEVICE_ID " +
                    "WHERE T.COUNTRY IN :country " +
                    "GROUP BY T.TESTER_ID",
            nativeQuery = true)
    List<Object[]> findBugCountForTesterByCounty(String[] country);

    @Query(value =
            "SELECT T.TESTER_ID, COUNT(B.BUG_ID) " +
                    "FROM TESTER T " +
                    "JOIN BUG B ON T.TESTER_ID = B.TESTER_ID " +
                    "JOIN DEVICE D ON B.DEVICE_ID = D.DEVICE_ID " +
                    "WHERE D.DESCRIPTION IN :device " +
                    "GROUP BY T.TESTER_ID",
            nativeQuery = true)
    List<Object[]> findBugCountForTesterByDevice(String[] device);

    @Query(value =
            "SELECT T.TESTER_ID, COUNT(B.BUG_ID) " +
                    "FROM TESTER T " +
                    "JOIN BUG B ON T.TESTER_ID = B.TESTER_ID " +
                    "GROUP BY T.TESTER_ID",
            nativeQuery = true)
    List<Object[]> findBugCountForTester();

}