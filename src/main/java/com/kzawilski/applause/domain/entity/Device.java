package com.kzawilski.applause.domain.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Device {
    @Id
    private Long deviceId;
    private String description;

}
