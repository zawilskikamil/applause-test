package com.kzawilski.applause.domain.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
public class Tester {
    @Id
    private Long testerId;
    private String firstName;
    private String lastName;
    private String country;
    private Date lastLogin;
}
