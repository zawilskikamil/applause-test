package com.kzawilski.applause.domain.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Bug {
    @Id
    private Long bugId;
    private Long deviceId;
    private Long testerId;
}
