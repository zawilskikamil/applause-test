package com.kzawilski.applause.domain.dto;

import com.kzawilski.applause.domain.entity.Tester;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TesterWithBugCountDTO {
    private Tester tester;
    private Long bugCount;
}
