package com.kzawilski.applause.rest;

import com.kzawilski.applause.domain.dto.TesterWithBugCountDTO;
import com.kzawilski.applause.service.BugCounterService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class TesterWithBugController {

    private BugCounterService bugCounterService;

    public TesterWithBugController(BugCounterService bugCounterService) {
        this.bugCounterService = bugCounterService;
    }

    @GetMapping("/testersWithBug")
    public ResponseEntity<List<TesterWithBugCountDTO>> getTesters(
            @RequestParam(required = false) String[] country,
            @RequestParam(required = false) String[] device) {
        return new ResponseEntity<>(bugCounterService.getTestersWithBugCount(country, device), HttpStatus.OK);
    }
}
