# Applause test app

### setup and run (using command line)
* make sure you have installed java 1.8 before  
* clone repository:  
`git clone https://zawilskikamil@bitbucket.org/zawilskikamil/applause-test.git`  
* you can run project using maven wrapper  
`cd applause-test`  
`./mvnw spring-boot:run` for linux/mac user  
or  
`./mvnw.cmd spring-boot:run` for windows user  


now you can open your favourite browser and search testers via link below:  
`http://localhost:8080/testersWithBug?country=[...]&device=[...]`  
for example:  
`http://localhost:8080/testersWithBug?country=GB&device=HTC%20One,iPhone%203,iPhone%205,iPhone%204,Nexus%204`  

and you get this response  
```json
[
    {
    "tester": {
        "testerId": 9,
        "firstName": "Darshini",
        "lastName": "Thiagarajan",
        "country": "GB",
        "lastLogin": "2013-08-05T13:00:38.000+0000"
    },
    "bugCount": 58
    },
    {
    "tester": {
        "testerId": 3,
        "firstName": "Leonard",
        "lastName": "Sutton",
        "country": "GB",
        "lastLogin": "2013-07-16T19:17:28.000+0000"
    },
    "bugCount": 59
    },
    {
    "tester": {
        "testerId": 6,
        "firstName": "Stanley",
        "lastName": "Chen",
        "country": "GB",
        "lastLogin": "2013-08-04T19:57:38.000+0000"
    },
    "bugCount": 110
    }
]
```